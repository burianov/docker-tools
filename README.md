
[![pipeline status](https://gitlab.com/burianov/docker-tools/badges/main/pipeline.svg)](https://gitlab.com/burianov/docker-tools/-/commits/main) [![coverage report](https://gitlab.com/burianov/docker-tools/badges/main/coverage.svg)](https://gitlab.com/burianov/docker-tools/-/commits/main) [![Latest Release](https://gitlab.com/burianov/docker-tools/-/badges/release.svg)](https://gitlab.com/burianov/docker-tools/-/releases)

## Custom Docker Images
## Ubuntu
with latest updates and with software-properties-common
```
Ubuntu 22.04.3 LTS (Jammy Jellyfish)
```
## GeoIP converted to dat MaxMind database
```
https://mailfud.org/geoip-legacy/
```
## OpenSSL
### linux/amd64
```
OpenSSL 3.2.0-dev  (Library: OpenSSL 3.2.0-dev )
compiler: gcc -fPIC -pthread -m64 -Wa,--noexecstack -Wall -O3 -DOPENSSL_USE_NODELETE -DL_ENDIAN -DOPENSSL_PIC -DOPENSSL_BUILDING_OPENSSL -DZLIB -DNDEBUG
OPENSSLDIR: "/usr/local/ssl"
ENGINESDIR: "/usr/local/ssl/lib64/engines-3"
MODULESDIR: "/usr/local/ssl/lib64/ossl-modules"
Seeding source: os-specific
CPUINFO: OPENSSL_ia32cap=0xfff83203078bffff:0x209c01ab
Providers:
  default
    name: OpenSSL Default Provider
    version: 3.2.0
    status: active
```
### linux/arm64/v8
```
OpenSSL 3.2.0-alpha3-dev  (Library: OpenSSL 3.2.0-alpha3-dev )
built on: Fri Oct  6 11:24:31 2023 UTC
platform: linux-aarch64
options:  bn(64,64)
compiler: gcc -fPIC -pthread -Wa,--noexecstack -Wall -O3 -DOPENSSL_USE_NODELETE -DOPENSSL_PIC -DOPENSSL_BUILDING_OPENSSL -DZLIB -DNDEBUG
OPENSSLDIR: "/usr/local/ssl"
ENGINESDIR: "/usr/local/ssl/lib/engines-3"
MODULESDIR: "/usr/local/ssl/lib/ossl-modules"
Seeding source: os-specific
CPUINFO: OPENSSL_armcap=0x81
Providers:
  default
    name: OpenSSL Default Provider
    version: 3.2.0
    status: active
```
## cURL
### linux/amd64
```
curl 8.4.1-DEV (x86_64-pc-linux-gnu) libcurl/8.4.1-DEV OpenSSL/3.0.10 zlib/1.2.11 brotli/1.0.9 zstd/1.4.8 c-ares/1.18.1 libidn2/2.3.2 libpsl/0.21.0 (+libidn2/2.3.2) libssh2/1.10.0 nghttp2/1.56.0 ngtcp2/0.19.1 nghttp3/0.15.0 librtmp/2.3 OpenLDAP/2.5.16
Protocols: dict file ftp ftps gopher gophers http https imap imaps ldap ldaps mqtt pop3 pop3s rtmp rtsp scp sftp smb smbs smtp smtps telnet tftp
Features: alt-svc AsynchDNS brotli HSTS HTTP2 HTTP3 HTTPS-proxy IDN IPv6 Largefile libz NTLM PSL SSL threadsafe TLS-SRP UnixSockets zstd
```
### linux/arm64/v8
```
curl 8.4.1-DEV (aarch64-unknown-linux-gnu) libcurl/8.4.1-DEV OpenSSL/3.0.10 zlib/1.2.11 brotli/1.0.9 zstd/1.4.8 c-ares/1.18.1 libidn2/2.3.2 libpsl/0.21.0 (+libidn2/2.3.2) libssh2/1.10.0 nghttp2/1.56.0 ngtcp2/0.19.1 nghttp3/0.15.0 librtmp/2.3 OpenLDAP/2.5.16
Protocols: dict file ftp ftps gopher gophers http https imap imaps ldap ldaps mqtt pop3 pop3s rtmp rtsp scp sftp smb smbs smtp smtps telnet tftp
Features: alt-svc AsynchDNS brotli HSTS HTTP2 HTTP3 HTTPS-proxy IDN IPv6 Largefile libz NTLM PSL SSL threadsafe TLS-SRP UnixSockets zstd
```
